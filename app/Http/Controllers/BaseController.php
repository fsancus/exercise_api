<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;

/**
 * Base controller
 */
class BaseController extends Controller
{
    /**
     * Response it's ok
     *
     * @param $data
     * @param $userId
     * @param $httpcode
     * @param $headers
     */
    protected function responseOk($data = null, $userId = null, $httcode = Response::HTTP_OK, $headers = [])
    {        
        if ($data) {
            $response = $data;
        }

        return response()->json($response, $httcode, $headers);
    }

    /**
     * Invalid response
     *
     * @param $error
     * @param $httcode
     */
    protected function responseKO($error = null, $httcode = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        if ($error) {
            $response['error'] = $error;
        }
        return response()->json($response, $httcode);
    }
}
