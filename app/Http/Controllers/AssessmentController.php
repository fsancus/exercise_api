<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\IAssessmentService;
use Illuminate\Http\Request;

class AssessmentController extends BaseController
{

    protected $assessmentService;

    /**
     * Create a new usersController instance.
     *
     * @return void
     */
    public function __construct(
        IAssessmentService $assessmentService
    ){
        $this->assessmentService      = $assessmentService;
    }

    /**
     * Get all questions
     *
     *  @return App\Models\components\schemas\QuestionList
     */
    public function questions(Request $request)
    {
        try {
            $questionList = $this->assessmentService->list($request->lang);
            return $this->responseOk(['description'=>'List of translated questions and associated choices', 'content'=>$questionList]);
        } catch (\Exception $ex) {
            return $this->responseKO([$ex->getMessage()]);
        }
    }

    
    /**
     * Create a new question
     *
     *  @return App\Models\components\schemas\Question
     */
    public function create(Request $request)
    {
        try {
            $question = $this->assessmentService->create($request->all());
            return $this->responseOk(['description'=>'Question and associated choices (not translated)', 'content'=>$question]);
        } catch (\Exception $ex) {
            return $this->responseKO([$ex->getMessage()]);
        }
    }
}
