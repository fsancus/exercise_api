<?php

namespace App\Repositories\Repositories;

use App\Models\BaseModel;
use App\Models\components\schemas\Choice;
use App\Models\components\schemas\Question;
use App\Models\components\schemas\QuestionList;
use App\Repositories\Interfaces\IAssessmentRepositorie;
use Exception;

/**
 * Class AssessmentRepositorie
 */

class AssessmentRepositorie extends BaseModel implements IAssessmentRepositorie
{
    // Model inyection for future assessment evolutions (MySQL, SQLite, etc...)
    private $questionList;
    private $question;
    private $choice;

    /**
     * @return void
     */
    public function __construct(
        QuestionList $questionList,
        Question $question,
        Choice $choice)
    {
        $this->questionList = $questionList;
        $this->question     = $question;
        $this->choice       = $choice;
    }

    /**
     * Get all questions
     *
     *
     *  @return App\Models\components\schemas\QuestionList
     */
    public function list()
    {
        //Check database and read file
        $database  = config('app.db_connection');
        switch ($database) {
            //READ FROM CSV FILE
            case 'csv':
                $questionListReaded = $this->readFromCSV();
                break;

            //READ FROM JSON FILE
            case 'json':
                $questionListReaded = $this->readFromJSON();
                break;

            //ADD MORE CASE TO ANOTHER DATABASES
            default:
                $questionListReaded = [];
                break;
        }
        return $questionListReaded;
    }

    /**
     * Save question
     *
     *
     *  @param Array
     */
    public function saveQuestion(Array $question)
    {
        //Check database and read file
        $database  = config('app.db_connection');
        switch ($database) {
            //SAVE TO CSV FILE
            case 'csv':
                if(!$this->saveToCSV($question))
                    throw new Exception('cannot save response in CSV file');
                break;

            //SAVE TO JSON FILE
            case 'json':
                if(!$this->saveToJSON($question))
                    throw new Exception('cannot save response in JSON file');
                break;

            //ADD MORE CASE TO ANOTHER DATABASES
            default:
                $questionListReaded = [];
                break;
        }
        return $question;

    }

    /**
     * Read questions from json file
     *
     *
     *  @return App\Models\components\schemas\QuestionList
     */
    public function readFromJSON()
    {
        $file = file_get_contents(config('database.connections.json.database_path'));
        $questionList = json_decode($file, true);
        return $questionList;
    }

    
    /**
     * Save question to json file
     *
     *
     *  @param Array
     */
    public function saveToJSON(Array $question)
    {
        $file = file_get_contents(config('database.connections.json.database_path'));
        $questionList = json_decode($file, true);
        array_push($questionList, $question);
        return file_put_contents(config('database.connections.json.database_path'), json_encode($questionList));
    }

    /**
     * Read questions from csv file
     *
     *
     *  @return App\Models\components\schemas\QuestionList
     */
    public function readFromCSV()
    {
        $file_open = fopen(config('database.connections.csv.database_path'), 'r');
        $buffer = array();
        ini_set('auto_detect_line_endings',TRUE);
        while (($data = fgetcsv($file_open)) !== false) {
            if ($data[0] != "Question text")  {
                $buffer[] = $data;
            }
        }
        $questionList = [];
        foreach ($buffer as $value) {
            $question = [
                'text' => $value[0],
                'createdAt' => $value[1],
                'choices'=>[
                    ['text' => $value[2]],
                    ['text' => $value[3]],
                    ['text' => $value[4]]
                ]
            ];
            $questionList[] = $question;
        }

        return $questionList;
    }

    /**
     * Save question to csv file
     *
     *  @param Array
     */
    public function saveToCSV(Array $question)
    {
        $file = fopen(config('database.connections.csv.database_path'), 'a');

        $newLine[] = $question['text'];
        $newLine[] = $question['createdAt'];
        $newLine[] = $question['choices'][0]['text'];
        $newLine[] = $question['choices'][1]['text'];
        $newLine[] = $question['choices'][2]['text'];
        fwrite($file,  PHP_EOL . '"'.implode('", "',$newLine). '"');

        return fclose($file);
    }
}