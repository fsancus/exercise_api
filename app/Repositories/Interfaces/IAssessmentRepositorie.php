<?php

namespace App\Repositories\Interfaces;

/**
 * Interface IAssessmentRepositorie
 */

interface IAssessmentRepositorie
{    
    /**
     * Get all questions
     *
     *
     *  @return Array
     */
    public function list();

    /**
     * Save question
     *
     *
     *  @param Array
     */
    public function saveQuestion(Array $question);

    /**
     * Read questions from json file
     *
     *
     *  @return Array
     */
    public function readFromJSON();

    /**
     * Save question to json file
     *
     *
     *  @param Array
     */
    public function saveToJSON(Array $question);

    /**
     * Read questions from csv file
     *
     *
     *  @return App\Models\components\schemas\QuestionList
     */
    public function readFromCSV();

    
    /**
     * Save question to csv file
     *
     *
     *  @param Array
     */
    public function saveToCSV(Array $question);

    
}