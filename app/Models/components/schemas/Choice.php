<?php

namespace App\Models\components\schemas;

use App\Models\BaseModel;

class Choice extends BaseModel
{
    protected $fillable = [

      'text',

    ];
}
