<?php

namespace App\Models\components\schemas;

use App\Models\BaseModel;

class QuestionList extends BaseModel
{
    /*
     *  QuestionList has many questions
     *  
     *  @return array[App\Models\components\schemas\Question]
     */    
    public function questions()
    {
        return $this->hasMany('App\Models\components\schemas\Question');
    }
}
