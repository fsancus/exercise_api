<?php

namespace App\Models\components\schemas;

use App\Models\BaseModel;

class Question extends BaseModel
{
    protected $fillable = [

      'text',
      'createdAt',

    ];

    /*
     *  Question has many choices
     *  
     *  @return array[App\Models\components\schemas\Choice]
     */   
    public function choices()
    {
        return $this->hasMany('App\Models\components\schemas\Choice');
    }
}
