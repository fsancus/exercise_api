<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

/**
 * Base model with commons constants and methods
 */
class BaseModel extends Authenticatable
{
    /**
     * a common rule for required fields validations
     */
    const REQUIRED      = 'required';

    /**
     * common field for use
     */
    const PREFIJCNT     = 'prefijcnt';

    /**
     * for any calificacion
     */
    const CALIF_FINISHED 	= 1;
    const CALIF_NOFINISHED 	= 0;


    const CAST_INTEGER      = 'integer';
    const CAST_REAL         = 'real';
    const CAST_FLOAT        = 'float';
    const CAST_DOUBLE       = 'double';
    const CAST_STRING       = 'string';
    const CAST_BOOLEAN      = 'boolean';
    const CAST_OBJECT       = 'object';
    const CAST_ARRAY        = 'array';
    const CAST_COLLECTION   = 'collection';
    const CAST_DATE         = 'date';
    const CAST_DATETIME     = 'datetime';
    const CAST_TIMESTAMP    = 'timestamp';

    /**
     * we tell laravel that we will manage the timestamp
     *
     * @var boolean
     */
    public $timestamps = false;


    public static function boot()
    {
        parent::boot();
    }

}
