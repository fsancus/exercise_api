<?php

namespace App\Services\Interfaces;

/**
 * Interface IAssessmentService
 */

interface IAssessmentService
{
    
    /**
     * Get all questions
     *
     *  @param string $language
     *  @return Array
     */
    public function list(string $language);
    
    /**
     * Create new question
     *
     *  @param Array $question
     *  @return Array
     */
    public function create(Array $question);

    /**
     * Translate texts
     *
     *  @param string $language
     *  @param Array $questionList
     *  @return App\Models\components\schemas\QuestionList
     */
    public function translate(Array $questionList, string $language);
}