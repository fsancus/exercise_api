<?php

namespace App\Services\Services;

use App\Repositories\Interfaces\IAssessmentRepositorie;
use App\Services\Interfaces\IAssessmentService;
use Exception;
use Stichoza\GoogleTranslate\GoogleTranslate;

/**
 * Class UsersService
 */

class AssessmentService implements IAssessmentService
{
    private $assessmentRepositorie;
    private $googleTranslate;
    /**
     * Constructor
     */
    public function __construct(
        IAssessmentRepositorie $assessmentRepositorie,
        GoogleTranslate $googleTranslate
    ) {
        $this->assessmentRepositorie = $assessmentRepositorie;
        $this->googleTranslate = $googleTranslate;
    }

    /**
     * Get all questions
     *
     *  @param string $language
     *  @return App\Models\components\schemas\QuestionList
     */
    public function list(string $language)
    {
        $valitador = \Validator::make(['lang'=>$language], [
            'lang' => 'string|required|max:2|min:2',
            ]);

        if ($valitador->fails()) {
            throw new Exception("ERRORS: ".implode('. ',$valitador->errors()->all()));
        } else {
            $ql = [];
            $ql = $this->assessmentRepositorie->list();        
            return $this->translate($ql, $language);
        }
    }

    /**
     * Create new question
     *
     *  @param Array $question
     *  @return App\Models\components\schemas\Question
     */
    public function create(Array $question)
    {
        $valitador = \Validator::make($question, [
            'text' => 'string|required',
            'createdAt' => 'date_format:"Y-m-d H:i:s"|required',
            'choices' => 'array|required|size:3',
            'choices.*.text' => 'string|required'
            ]);

        if ($valitador->fails()) {
            throw new Exception("ERRORS: ".implode('. ',$valitador->errors()->all()));
        } else {        
            $this->assessmentRepositorie->saveQuestion($question);
            return $question;
        }
    }

    /**
     * Translate texts
     *
     *  @param string $language
     *  @param Array $questionList
     *  @return Array
     */
    public function translate(Array $questionList, string $language)
    {
        $this->googleTranslate->setTarget($language);
        foreach ($questionList as $i => $question) {
            // Translate question text
            $questionList[$i]['text'] = $this->googleTranslate->translate($question['text']);
            foreach ($question['choices'] as $j => $choice) {
                //Translate choice text
                $questionList[$i]['choices'][$j]['text'] = $this->googleTranslate->translate($choice['text']);
            }
        }
        return $questionList;
    }
}