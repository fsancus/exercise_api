<?php

namespace Tests\Feature\Assessment;

use Tests\TestCase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Symfony\Component\HttpFoundation\Response;
use Mockery;

class AssessmentControllerTest extends TestCase
{
    use WithoutMiddleware, InteractsWithSession;
    /**
     * A basic feature test example.
     *
     */
    protected $assessmentService;
    protected $exception;

    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
        $this->assessmentService    = Mockery::mock('App\Services\Services\AssessmentService');
        $this->exception            = Mockery::mock('\Exception');

        $this->app->instance('App\Services\Services\AssessmentService', $this->assessmentService);
        $this->app->instance('\Exception', $this->exception);
    }

    public function test_can_getQuestions()
    {
        $this->assessmentService->shouldReceive('list')->with('fr')->andReturn(array());
        $response = $this->call('GET', 'api/questions',['lang'=>'fr']);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    }

    public function test_can_getQuestions_exception()
    {
        $this->assessmentService->shouldReceive('list')->with('fr')->andThrow($this->exception);
        $response = $this->call('GET', 'api/questions',['lang'=>'fr']);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->status());
        $response->assertJsonStructure(['error']);
    }

    public function test_can_createQuestion()
    {
        $question = 
        ['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]];

        $this->assessmentService->shouldReceive('create')->with($question)->andReturn(array());
        $response = $this->call('POST', 'api/questions',$question);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    }

    public function test_can_createQuestion_exception()
    {
        $question = 
        ['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]];

        $this->assessmentService->shouldReceive('create')->with($question)->andThrow($this->exception);
        $response = $this->call('POST', 'api/questions',$question);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->status());
        $response->assertJsonStructure(['error']);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
        app()->forgetInstances();
    }
}
