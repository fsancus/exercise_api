<?php

namespace Tests\Feature\Users;

use Tests\TestCase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Symfony\Component\HttpFoundation\Response;
use Mockery;

class AssessmentServiceTest extends TestCase
{
    use WithoutMiddleware, InteractsWithSession;
    /**
     * A basic feature test example.
     *
     */
    protected $assessmentRepositorie;
    protected $exception;
    protected $googleTranslate;

    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
        $this->assessmentRepositorie = Mockery::mock('App\Repositories\Repositories\AssessmentRepositorie');
        $this->exception             = Mockery::mock('\Exception');
        $this->googleTranslate       = Mockery::mock('Stichoza\GoogleTranslate\GoogleTranslate');
        
        $this->app->instance('App\Repositories\Repositories\AssessmentRepositorie', $this->assessmentRepositorie);
        $this->app->instance('\Exception', $this->exception);
        $this->app->instance('Stichoza\GoogleTranslate\GoogleTranslate', $this->googleTranslate);
    }    

    public function test_can_getQuestions()
    {
        $questionList = 
        [['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]]];

        $this->googleTranslate->shouldReceive('setTarget')->with('fr')->andReturn($this->googleTranslate);
        $this->googleTranslate->shouldReceive('translate')->andReturn('fake text translate');
        $this->assessmentRepositorie->shouldReceive('list')->andReturn($questionList);
        $response = $this->call('GET', 'api/questions',['lang'=>'fr']);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    }

    public function test_can_getQuestions_validation_fails()
    {
        $questionList = 
        [['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]]];

        $this->googleTranslate->shouldReceive('setTarget')->with('fr')->andReturn($this->googleTranslate);
        $this->googleTranslate->shouldReceive('translate')->andReturn('fake text translate');
        $this->assessmentRepositorie->shouldReceive('list')->andReturn($questionList);
        $response = $this->call('GET', 'api/questions',['lang'=>00]);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->status());
        $response->assertJsonStructure(['error']);
    }    

    public function test_can_createQuestion()
    {
        $question = 
        ['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]];

        $this->assessmentRepositorie->shouldReceive('saveQuestion')->with($question)->andReturn(true);
        $response = $this->call('POST', 'api/questions',$question);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    } 

    public function test_can_createQuestion_validation_fails()
    {
        $question = 
        ['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue']
            ]];

        $this->assessmentRepositorie->shouldReceive('saveQuestion')->with($question)->andReturn(true);
        $response = $this->call('POST', 'api/questions',$question);
        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->status());
        $response->assertJsonStructure(['error']);
    }



    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
        app()->forgetInstances();
    }
}
