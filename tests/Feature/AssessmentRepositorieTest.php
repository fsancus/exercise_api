<?php

namespace Tests\Feature\Assessment;

use Tests\TestCase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Symfony\Component\HttpFoundation\Response;
use Mockery;

class AssessmentRepositorieTest extends TestCase
{
    use WithoutMiddleware, InteractsWithSession;
    /**
     * A basic feature test example.
     *
     */
    
    protected $exception;
    protected $googleTranslate;

    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
        $this->exception    = Mockery::mock('\Exception');
        $this->googleTranslate       = Mockery::mock('Stichoza\GoogleTranslate\GoogleTranslate');
         
        $this->app->instance('\Exception', $this->exception);
        $this->app->instance('Stichoza\GoogleTranslate\GoogleTranslate', $this->googleTranslate);
    }    

    public function test_can_getQuestions()
    {
        $this->googleTranslate->shouldReceive('setTarget')->with('fr')->andReturn($this->googleTranslate);
        $this->googleTranslate->shouldReceive('translate')->andReturn('fake text translate');


        $response = $this->call('GET', 'api/questions',['lang'=>'fr']);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    }

    
    public function test_can_createQuestion()
    {
        $question = 
        ['text'=>'two plus two?',
        'createdAt'=>'2019-06-02 00:00:00',
        'choices'=> [
            ['text' =>'five'],
            ['text' => 'blue'],
            ['text' => 'four']
            ]];

        $response = $this->call('POST', 'api/questions',$question);
        $this->assertEquals(Response::HTTP_OK, $response->status());
        $response->assertJsonStructure(['description', 'content']);
    } 


    public function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
        app()->forgetInstances();
    }
}
